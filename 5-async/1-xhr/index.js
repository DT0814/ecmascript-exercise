function fetchData(url, successCallback, errorCallback) {
  const xhr = new XMLHttpRequest();
  // <-- start
  // TODO 21: 通过XMLHttpRequest实现异步请求
  xhr.onerror = () => {
    errorCallback(xhr.response);
  };
  xhr.onload = () => {
    console.log(xhr.response);
    successCallback(xhr.response);
  };
  xhr.open('GET', url);
  xhr.send();
  // end -->
}

const URL = 'http://localhost:3000/api';
fetchData(
  URL,
  result => {
    document.writeln(JSON.parse(result).name);
  },
  error => {
    console.error(error);
  }
);
