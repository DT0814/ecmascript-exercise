export default function countTypesNumber(source) {
  // TODO 6: 在这里写实现代码
  let num = 0;
  const entries = Object.entries(source);
  entries.forEach(([key, value]) => {
    num += Number.parseInt(value);
  });
  return num;
}
