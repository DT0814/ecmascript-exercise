function chooseMultiplesOfThree(collection) {
  // TODO 1: 在这里写实现代码
  return collection.filter(v => v % 3 === 0);
}

function chooseNoRepeatNumber(collection) {
  // TODO 2: 在这里写实现代码
  return collection.filter((v, ind, self) => {
    return ind === self.indexOf(v);
  });
}

export { chooseMultiplesOfThree, chooseNoRepeatNumber };
