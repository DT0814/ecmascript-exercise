// TODO 20: 在这里写实现代码
import Person from './person';

class Teacher extends Person {
  constructor(name, age, klass) {
    super(name, age);
    this.klass = klass;
  }

  introduce() {
    const res = `${super.introduce()} I am a Teacher. I teach`;
    if (typeof this.klass === 'undefined') {
      return res + ' No Class.';
    }
    return res + ` Class ${this.klass}.`;
  }
}

export default Teacher;
